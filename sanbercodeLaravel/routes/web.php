<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\dataTableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 


Route::get('/table', [TableController::class, 'tables']);
Route::get('/dataTable', [dataTableController::class, 'dataTables']);

// create atau tambah cast
Route::get('/cast/create', [CastController::class, 'create']); 

// untuk kirim data ke database
Route::post('/cast', [CastController::class, 'castFilm']);

// read atau baca data atau tampil data
Route::get('/cast', [CastController::class, 'index']);

// menampilkan detail data
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update atau edit data
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

// update data ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// delete data berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


