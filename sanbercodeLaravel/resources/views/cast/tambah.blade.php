@extends('layout.master')

@section ('judul')
Halaman Tambah Cast
@endsection

@section ('judulContent')

<form action="/cast" method="POST">
    @csrf
    <div class="mb-3">
      <label class="form-label">Nama Cast</label>
      <input type="name" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="mb-3">
      <label class="form-label">Umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    @error('judulFilm')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="mb-3">
      <label class="form-label">Biodata</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection