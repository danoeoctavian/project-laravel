@extends('layout.master')

@section ('judul')
Edit Cast
@endsection

@section ('judulContent')
Edit Cast Film
@endsection

@section('primaryContent')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label class="form-label">Nama Cast</label>
      <input type="name" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="mb-3">
      <label class="form-label">Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('judulFilm')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="mb-3">
      <label class="form-label">Biodata</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection