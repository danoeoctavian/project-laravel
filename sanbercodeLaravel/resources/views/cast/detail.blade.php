@extends('layout.master')

@section ('judul')
Detail Cast
@endsection

@section ('judulContent')
Detail Cast Film
@endsection

@section('primaryContent')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-info btn-sm">Kembali</a>

@endsection